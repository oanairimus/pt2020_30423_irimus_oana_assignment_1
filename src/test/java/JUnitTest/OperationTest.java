package JUnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

//import org.junit.jupiter.api.Test;

import junit.framework.Assert;
import monomial.Monomial;
import monomial.Polynomial;
import polynomialsOperations.Operation;

@SuppressWarnings("deprecation")
public class OperationTest {

	@SuppressWarnings("deprecation")
	@Test
	public void testCompute() {
		
		String expected = "";
		String actual = "";
		
		Monomial m00 = new Monomial(0,2);
		Monomial m01 = new Monomial(2,3);
		Monomial m02 = new Monomial(3,1);
		
		Monomial m10 = new Monomial(1,1);
		Monomial m11 = new Monomial(2,2);
		Monomial m12 = new Monomial(3,1);
		Monomial m13 = new Monomial(4,4);
		
		List<Monomial> l0 = new ArrayList<>();
		List<Monomial> l1 = new ArrayList<>();
		
		l0.add(m00);
		l0.add(m01);
		l0.add(m02);
		
		l1.add(m10);
		l1.add(m11);
		l1.add(m12);
		l1.add(m13);
		
		Polynomial p0 = new Polynomial(l0, 3);
		Polynomial p1 = new Polynomial(l1, 4);
		
		Operation o = new Operation();
		
		actual = o.compute(p0, "+", p1);
		expected = "2.0x^0 + 1.0x^1 + 5.0x^2 + 2.0x^3 + 4.0x^4.";
		Assert.assertEquals(expected, actual);
		
		actual = o.compute(p0, "-", p1);
		expected = "2.0x^0 + -1.0x^1 + -5.0x^2 + -2.0x^3 + -4.0x^4.";
		Assert.assertEquals(expected, actual);
		
		actual = o.compute(p0, "*", p1);
		expected = "2.0x^1 + 4.0x^2 + 5.0x^3 + 15.0x^4 + 5.0x^5 + 13.0x^6 + 4.0x^7.";
		Assert.assertEquals(expected, actual);
		
		actual = o.derivate(p0);
		expected = "6.0x^1+3.0x^2.";
		Assert.assertEquals(expected, actual);
		
		actual = o.derivate(p1);
		expected = "1.0x^0+4.0x^1+3.0x^2+16.0x^3.";
		Assert.assertEquals(expected, actual);
		
		actual = o.integrate(p0);
		expected = "2.0x^1+1.0x^3+0.25x^4 + C";
		Assert.assertEquals(expected, actual);
		
		actual = o.integrate(p1);
		expected = "0.5x^2+0.6666667x^3+0.25x^4+0.8x^5 + C";
		Assert.assertEquals(expected, actual);
		
		Monomial m20 = new Monomial(0,2);
		Monomial m21 = new Monomial(2,3);
		Monomial m22 = new Monomial(3,1);
		Monomial m23 = new Monomial(5, -6);
		Monomial m24 = new Monomial(6, 23);
		
		Monomial m30 = new Monomial(1,-1);
		Monomial m31 = new Monomial(2,-2);
		Monomial m32 = new Monomial(4,-11);
		Monomial m33 = new Monomial(5,14);
		
		List<Monomial> l2 = new ArrayList<>();
		List<Monomial> l3 = new ArrayList<>();
		
		l2.add(m20);
		l2.add(m21);
		l2.add(m22);
		l2.add(m23);
		l2.add(m24);
		
		l3.add(m30);
		l3.add(m31);
		l3.add(m32);
		l3.add(m33);
		
		Polynomial p2 = new Polynomial(l2, 5);
		Polynomial p3 = new Polynomial(l3, 4);
		
		actual = o.compute(p2, "+", p3);
		expected = "2.0x^0 + -1.0x^1 + 1.0x^2 + 1.0x^3 + -11.0x^4 + 8.0x^5 + 23.0x^6.";
		Assert.assertEquals(expected, actual);
		
		actual = o.compute(p2, "-", p3);
		expected = "2.0x^0 + 1.0x^1 + -1.0x^2 + 1.0x^3 + 11.0x^4 + -8.0x^5 + 23.0x^6.";
		Assert.assertEquals(expected, actual);
		
		actual = o.compute(p2, "*", p3);
		expected = "-2.0x^1 + -4.0x^2 + -3.0x^3 + -29.0x^4 + 26.0x^5 + -27.0x^6 + 20.0x^7 + -32.0x^8 + 66.0x^9 + -337.0x^10 + 322.0x^11.";
		Assert.assertEquals(expected, actual);
		
		actual = o.derivate(p2);
		expected = "6.0x^1+3.0x^2+-30.0x^4+138.0x^5.";
		Assert.assertEquals(expected, actual);
		
		actual = o.derivate(p3);
		expected = "-1.0x^0+-4.0x^1+-44.0x^3+70.0x^4.";
		Assert.assertEquals(expected, actual);
		
		actual = o.integrate(p2);
		expected = "2.0x^1+1.0x^3+0.25x^4+-1.0x^6+3.2857144x^7 + C";
		Assert.assertEquals(expected, actual);
		
		actual = o.integrate(p3);
		expected = "-0.5x^2+-0.6666667x^3+-2.2x^5+2.3333333x^6 + C";
		Assert.assertEquals(expected, actual);			
		
		Monomial m40 = new Monomial(0,0);
		
		Monomial m50 = new Monomial(1,3);
		Monomial m51 = new Monomial(2,2);
		Monomial m52 = new Monomial(3, -44);
		
		List<Monomial> l4 = new ArrayList<>();
		List<Monomial> l5 = new ArrayList<>();
		
		l4.add(m40);
		l5.add(m50);
		l5.add(m51);
		l5.add(m52);
		
		Polynomial p4 = new Polynomial(l4, 1);
		Polynomial p5 = new Polynomial(l5, 3);
		
		actual = o.compute(p4, "+", p5);
		expected = "0.0x^0 + 3.0x^1 + 2.0x^2 + -44.0x^3.";
		Assert.assertEquals(expected, actual);
		
		actual = o.compute(p4, "-", p5);
		expected = "0.0x^0 + -3.0x^1 + -2.0x^2 + 44.0x^3.";
		Assert.assertEquals(expected, actual);
		
		actual = o.compute(p4, "*", p5);
		expected = "0.0x^1 + 0.0x^2 + 0.0x^3.";
		Assert.assertEquals(expected, actual);
		
		actual = o.derivate(p4);
		expected = "";
		Assert.assertEquals(expected, actual);
		
		actual = o.derivate(p5);
		expected = "3.0x^0+4.0x^1+-132.0x^2.";
		Assert.assertEquals(expected, actual);
		
		actual = o.integrate(p4);
		expected = "0.0x^1 + C";
		Assert.assertEquals(expected, actual);
		
		actual = o.integrate(p5);
		expected = "1.5x^2+0.6666667x^3+-11.0x^4 + C";
		Assert.assertEquals(expected, actual);
		System.out.println(actual);
	}

}
