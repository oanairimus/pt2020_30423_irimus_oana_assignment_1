package monomial;
import java.util.*;

public class Polynomial{
	private List<Monomial> monomials;
	private Integer degree;
	
	public Polynomial(List<Monomial> monomials, Integer numberOfTerms) {
		this.monomials = monomials;
		this.degree = numberOfTerms;
	}
	
	public Polynomial() {}

	public List<Monomial> getMonomials() {
		return monomials;
	}
	public void addToList(Monomial m) {
		this.monomials.add(m);
	}
	public void setMonomials(List<Monomial> m) {
		this.monomials = m;
	}
	public Integer getDegree() {
		return degree;
	}
	public void setDegree(Integer d) {
		this.degree = d;
	}
	
	public Monomial getLast() {
		Monomial m = new Monomial(this.monomials.get(this.monomials.size()-1).getPower(),this.monomials.get(this.monomials.size()-1).getCoefficient());
		return m;
	}
}
