package monomial;

public class Monomial {
	private Integer power;
	private float coefficient;
	
	public Monomial() {}
	
	public Monomial(Integer power, float coefficient) {
		this.setPower(power);
		this.setCoefficient(coefficient);
	}

	public Integer getPower() {
		return power;
	}

	public void setPower(Integer power) {
		this.power = power;
	}

	public float getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(float coefficient) {
		this.coefficient = coefficient;
	}
	
	
}
