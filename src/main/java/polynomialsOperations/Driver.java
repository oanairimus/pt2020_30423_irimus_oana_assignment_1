package polynomialsOperations;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import GUI.View;
import monomial.Monomial;
import monomial.Polynomial;


public class Driver {
	
	public static void main(String[] args) {
		Monomial m1 = new Monomial(0, 2);
		Monomial m12 = new Monomial(1, 3);
		Monomial m13 = new Monomial(2, 3);
		
		Monomial m2 = new Monomial(0, 1);
		Monomial m21 = new Monomial(1, 3);
		Monomial m22 = new Monomial(2, -13);
		Monomial m23 = new Monomial(3, 41);
		
		List<Monomial> l1 = new ArrayList<>();
		List<Monomial> l2 = new ArrayList<>();
		
		Operation a = new Operation();
		//Divider d = new Divider();
		
		l1.add(m1);
		l1.add(m12);
		l1.add(m13);
		
		l2.add(m2);
		l2.add(m21);
		l2.add(m22);
		l2.add(m23);
		
		Polynomial p1 = new Polynomial(l1, l1.size());
		Polynomial p2 = new Polynomial(l2, l2.size());

		System.out.println(a.compute(p1, "+", p2));
		System.out.println();
		
		View g = new View();
		g.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		g.setVisible(true);
		
	}

}

