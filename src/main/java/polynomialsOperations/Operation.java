package polynomialsOperations;
import java.util.*;

import monomial.Monomial;
import monomial.Polynomial;

public class Operation {
	private Polynomial result;
	
	public Polynomial getResult() {
		return result;
	}
	
	public void setResult(Polynomial result) {
		this.result = result;
	}
	
	public List<Monomial> addPolynomials(Polynomial p1, Polynomial p2){
		List<Monomial> mon = new ArrayList<>();
		HashMap<Integer, Integer> hm = new HashMap<>();
		
		Integer highestDeg = 0;
		
		for(Monomial m1 : p1.getMonomials()) {
			hm.put(m1.getPower(), (int) m1.getCoefficient());
			if(highestDeg < m1.getPower()) {
				highestDeg = m1.getPower();
			}
		}
		
		for(Monomial m2 : p2.getMonomials()) {
			if(hm.containsKey(m2.getPower())) {
				Integer coef = (int) (hm.get(m2.getPower()) + m2.getCoefficient());
				hm.replace(m2.getPower(), coef);
				if(highestDeg < m2.getPower()) {
					highestDeg = m2.getPower();
				}
			}
			else {
				hm.put(m2.getPower(), (int) m2.getCoefficient());
				if(highestDeg < m2.getPower()) {
					highestDeg = m2.getPower();
				}
			}
		}
		
		for(int i = 0; i <= highestDeg; i++) {
			if(hm.containsKey(i)) {
				Monomial aux = new Monomial(i, hm.get(i));
				mon.add(aux);
			}
		}
		
		return mon;
	}
	
	public List<Monomial> subtractPolynomials(Polynomial p1, Polynomial p2){
		List<Monomial> mon = new ArrayList<>();
		HashMap<Integer, Integer> hm = new HashMap<>();
		
		Integer highestDeg = 0;
		
		for(Monomial m1 : p1.getMonomials()) {
			hm.put(m1.getPower(), (int) m1.getCoefficient());
			if(highestDeg < m1.getPower()) {
				highestDeg = m1.getPower();
			}
		}
		
		for(Monomial m2 : p2.getMonomials()) {
			if(hm.containsKey(m2.getPower())) {
				Integer coef = 0- (int) (hm.get(m2.getPower()) + m2.getCoefficient());
				hm.replace(m2.getPower(), coef);
				if(highestDeg < m2.getPower()) {
					highestDeg = m2.getPower();
				}
			}
			else {
				hm.put(m2.getPower(), 0- (int) m2.getCoefficient());
				if(highestDeg < m2.getPower()) {
					highestDeg = m2.getPower();
				}
			}
		}
		
		for(int i = 0; i <= highestDeg; i++) {
			if(hm.containsKey(i)) {
				Monomial aux = new Monomial(i, hm.get(i));
				mon.add(aux);
			}
		}
		
		return mon;
	}
	
	public List<Monomial> multiplyPolynomials(Polynomial p1, Polynomial p2){
		HashMap<Integer, Integer> aux = new HashMap<>();
		Integer power = 0;
		Integer coef = 0;
		List<Monomial> result = new ArrayList<>();
		
		for(Monomial m1 : p1.getMonomials()) {
			for(Monomial m2 : p2.getMonomials()) {
				power = m1.getPower()+m2.getPower();
				coef = (int)(m1.getCoefficient()*m2.getCoefficient());
				if(aux.get(power) == null) {
					aux.put(power, (Integer)coef);			
				}
				else {
					coef += aux.get(power);
					aux.replace(power, coef);
				}
			}
		}
		int i = 0;
		while(!aux.isEmpty()) {
			Monomial mon = new Monomial();
			if(aux.get(i) != null) {
				mon.setCoefficient(aux.get(i));
				mon.setPower(i);
				result.add(mon);
				aux.remove(i);
			}
			i++;
		}
		return result;
	}
	
	public Integer highestDegree(Polynomial p) {
		Integer degree = 0;
		for(Monomial m : p.getMonomials()) {
			if(m.getPower() > degree) {
				degree = m.getPower();
			}
		}
		return degree;
	}
	
	public String derivate(Polynomial p1) {
		List<Monomial> newlist = new ArrayList<>();
		Integer deg = highestDegree(p1) - 1;
		
		for(Monomial m : p1.getMonomials()) {
			if(m.getPower() != 0) {		
				Integer p = m.getPower() - 1;
				Integer c = (int) (m.getCoefficient() *(p+1));	
				Monomial aux = new Monomial(p, c);
				newlist.add(aux);
			}
		}
		String res = new String();
		for(Monomial n : newlist) {
			res += n.getCoefficient() + "x^" + n.getPower();
			if(!(n.getPower() == deg)) {
				res += "+";
			}
			else {
				res += ".";
			}
		}
		return res;
	}
	
	public String integrate(Polynomial p1) {
		List<Monomial> newlist = new ArrayList<>();
		Integer deg = highestDegree(p1) + 1;
		
		for(Monomial m : p1.getMonomials()) {
			Integer p = m.getPower() + 1;
			float c = m.getCoefficient() / p;
			Monomial aux = new Monomial(p, c);
			newlist.add(aux);
		}
		String res = new String();
		for(Monomial n : newlist) {
			res += n.getCoefficient() + "x^" + n.getPower();
			if(!(n.getPower() == deg)) {
				res += "+";
			}
			else {
				res += " + C";
			}
		}
		return res;
	}
	
	public String compute(Polynomial p1, String sign, Polynomial p2) {
		Polynomial res = new Polynomial();
		Operation a = new Operation();
		String sum = new String();
		
		if(sign.equals("+")) {
			res.setMonomials(a.addPolynomials(p1, p2));
		}
		else if(sign.equals("-")) {
			res.setMonomials(a.subtractPolynomials(p1, p2));
		}
		else if(sign.equals("*")) {
			res.setMonomials(a.multiplyPolynomials(p1, p2));
		}
		
		int i = 0;
		while(i < res.getMonomials().size()) {
			if(i < res.getMonomials().size() - 1) {
				sum += res.getMonomials().get(i).getCoefficient() + "x^" + res.getMonomials().get(i).getPower() + " + ";
			}
			else {
				sum += res.getMonomials().get(i).getCoefficient() + "x^" + res.getMonomials().get(i).getPower() + ".";
			}
			i++;
		}
		return sum;
	}	
	
	
}