package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import monomial.Monomial;
import monomial.Polynomial;

public class Controller implements ActionListener{
	
	private View view;
	private Logic logic = new Logic();
	
	public Controller (View view) {
		this.view = view;
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == view.getSubtr()) {
			view.setStoreP1(view.getP1text());
			view.setStoreP2(view.getP2text());
			
			Polynomial p1 = new Polynomial(getInputAsList(view.getStoreP1()), getInputAsList(view.getStoreP1()).size());
			Polynomial p2 = new Polynomial(getInputAsList(view.getStoreP2()), getInputAsList(view.getStoreP2()).size());
			
			JOptionPane.showMessageDialog(null, logic.sub(p1, p2));
		}
		if(e.getSource() == view.getAdd()) {
			view.setStoreP1(view.getP1text());
			view.setStoreP2(view.getP2text());
			
			Polynomial p1 = new Polynomial(getInputAsList(view.getStoreP1()), getInputAsList(view.getStoreP1()).size());
			Polynomial p2 = new Polynomial(getInputAsList(view.getStoreP2()), getInputAsList(view.getStoreP2()).size());
			
			JOptionPane.showMessageDialog(null, logic.add(p1, p2));
		}
		if(e.getSource() == view.getMultiply()) {
			view.setStoreP1(view.getP1text());
			view.setStoreP2(view.getP2text());
			
			Polynomial p1 = new Polynomial(getInputAsList(view.getStoreP1()), getInputAsList(view.getStoreP1()).size());
			Polynomial p2 = new Polynomial(getInputAsList(view.getStoreP2()), getInputAsList(view.getStoreP2()).size());		
						
			JOptionPane.showMessageDialog(null, logic.mul(p1, p2));
		}
		if(e.getSource() == view.getDeriv()) {
			view.setStoreP1(view.getP1text());
			
			Polynomial p1 = new Polynomial(getInputAsList(view.getStoreP1()), getInputAsList(view.getStoreP1()).size());
		
			JOptionPane.showMessageDialog(null, logic.deriv(p1));
		}
		if(e.getSource() == view.getIntegr()) {
			view.setStoreP1(view.getP1text());
			
			Polynomial p1 = new Polynomial(getInputAsList(view.getStoreP1()), getInputAsList(view.getStoreP1()).size());
			
			JOptionPane.showMessageDialog(null, logic.integ(p1));
		}
	}
	
	public List<Monomial> getInputAsList(String s){
		Integer[] splitted = new Integer[s.length()];
		Integer index = 0;
		List<Monomial> result = new ArrayList<>();
		int i = 0;
		
		while(i < s.length()) {
			if(s.charAt(i) >= 48 && s.charAt(i) <= 57) {
				if(i-1 >= 0 && s.charAt(i-1) == '-') {
					splitted[index] = s.charAt(i) - 48;
					splitted[index] = 0 - splitted[index];
					
					while(i+1 < s.length() && s.charAt(i+1) != '*' && s.charAt(i+1) != '+' && s.charAt(i+1) != '-') {
						i++;
						splitted[index] = splitted[index]*10;
						splitted[index] += -1*(s.charAt(i) - 48);
					}
				}
				else {
					splitted[index] = s.charAt(i) - 48;
					while(i+1 < s.length() && s.charAt(i+1) != '*' && s.charAt(i+1) != '+' && s.charAt(i+1) != '-') {
						i++;
						splitted[index] = splitted[index]*10;
						splitted[index] += s.charAt(i) - 48;
					}
				}
				index++;
			}
			i++;
		}
		
		if(index == 0 || index % 2 != 0) {
			JOptionPane.showMessageDialog(null, "You entered an invalid polynomial. Please try again");
		}
		
		for(int j = 0; j < index - 1; j+=2) {
			if(j+1 < index) {
				Monomial m = new Monomial(splitted[j+1], splitted[j]);
				result.add(m);
			}
		}
		return result;
	}
	
	

}
