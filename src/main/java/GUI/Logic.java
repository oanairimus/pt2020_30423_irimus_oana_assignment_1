package GUI;

import monomial.Polynomial;
import polynomialsOperations.Operation;

public class Logic {
	
	Operation op = new Operation();
	
	public String add(Polynomial p1, Polynomial p2) {
		return op.compute(p1, "+", p2);
	}
	
	public String sub(Polynomial p1, Polynomial p2) {
		return op.compute(p1, "-", p2);
	}
	
	public String mul(Polynomial p1, Polynomial p2) {
		return op.compute(p1, "*", p2);
	}
	
	public String deriv(Polynomial p1) {
		return op.derivate(p1);
	}
	
	public String integ(Polynomial p1) {
		return op.integrate(p1);
	}
}
