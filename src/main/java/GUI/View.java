package GUI;

import javax.swing.*;

@SuppressWarnings("serial")
public class View extends JFrame {
	private JLabel enterP1;
	private JLabel enterP2;
	private JTextField p2;
	private JTextField p1;
	private JButton subtr;
	private JButton add;
	private JButton multiply;
	private JButton divide;
	private JButton deriv;
	private JButton integr;
	private String storeP1;
	private String storeP2;
	
	Controller controller = new Controller(this);
	
	public View() {
		setLayout(null);
		setSize(500, 450);
		
		enterP1 = new JLabel("P1");
		enterP2 = new JLabel("P2");
		subtr = new JButton("-");
		add = new JButton("+");
		deriv = new JButton("derivate");
		multiply = new JButton("*");
		divide = new JButton("/");
		integr = new JButton("integrate");
		p1 = new JTextField();
		p2 = new JTextField();
		
		enterP1.setBounds(10, 30, 20, 20);
		enterP2.setBounds(10, 60, 20, 20);
		p1.setBounds(30, 30, 160, 30);
		p2.setBounds(30, 60, 160, 30);
		subtr.setBounds(100, 100, 60, 30);
		add.setBounds(100, 130, 60, 30);
		multiply.setBounds(100, 160, 60, 30);
		divide.setBounds(170, 160, 60, 30);
		deriv.setBounds(100, 190, 120, 30);
		integr.setBounds(100, 210, 120, 30);
		
		subtr.addActionListener(controller);
		add.addActionListener(controller);
		multiply.addActionListener(controller);
		divide.addActionListener(controller);
		deriv.addActionListener(controller);
		integr.addActionListener(controller);
		
		add(subtr);
		add(add);
		add(multiply);
		add(divide);
		add(deriv);
		add(integr);
		add(p1);
		add(p2);
		add(enterP2);
		add(enterP1);
	}
	
	public String getP1text() {
		return p1.getText();
	}
	
	public String getP2text() {
		return p2.getText();
	}
	
	public JButton getSubtr() {
		return subtr;
	}
	public JButton getAdd() {
		return add;
	}
	public JButton getMultiply() {
		return multiply;
	}
	public JButton getDeriv() {
		return deriv;
	}
	public JButton getIntegr() {
		return integr;
	}
	public JButton getDivide() {
		return divide;
	}

	public void setStoreP1(String s) {
		this.storeP1 = s;
	}
	
	public void setStoreP2(String s) {
		this.storeP2 = s;
	}
	public String getStoreP1() {
		return storeP1;
	}	
	public String getStoreP2() {
		return storeP2;
	}
	
}
